<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<helpset version="1.0">
<maps>
<mapref location="Map.jhm"/>
<homeID>Help.html</homeID>
</maps>
<title>Help</title>
<view>
<name>TOC</name>
<label>TOC Of Contents</label>
<type>javax.help.TOCView</type>
<data>TOC.xml</data>
</view>
<view>
<name>Index</name>
<label>Index</label>
<type>javax.help.IndexView</type>
<data>Index.xml</data>
</view>
<view>
<name>Search</name>
<label>Search</label>
<type>javax.help.SearchView</type>
<data engine="com.sun.java.help.search.DefaultSearchEngine">JavaHelpSearch</data>
</view>
</helpset>
