﻿Imports System.IO
Imports System.Text.RegularExpressions
Imports System.Runtime.InteropServices

Public Class IDE

    Private Sub COMPILARToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles COMPILARToolStripMenuItem.Click
        MessageBox.Show("Aun no esta implementado.")
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub NuevoToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoToolStripMenuItem.Click
        Dim tabPage As TabPage = New TabPage()
        tabPage.Name = "Nuevo" & TabControl1.TabPages.Count + 1
        tabPage.Text = "Nuevo" & TabControl1.TabPages.Count + 1
        tabPage.Parent = TabControl1
        tabPage.Dock = DockStyle.Fill
        Dim rcbx = New RichTextBox()
        rcbx.Parent = tabPage
        rcbx.Dock = DockStyle.Fill
        AddHandler rcbx.TextChanged, AddressOf rcbx_TextChanged
        tabPage.Controls.Add(rcbx)
    End Sub

    Private Sub AbrirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirToolStripMenuItem.Click
        Dim dialog As OpenFileDialog = New OpenFileDialog()
        If dialog.ShowDialog() = DialogResult.OK Then
            Dim filePath As String = dialog.FileName
            Dim tabPage As TabPage = New TabPage()
            tabPage.Name = Path.GetFileName(filePath)
            tabPage.Text = Path.GetFileName(filePath)
            tabPage.Parent = TabControl1
            tabPage.Dock = DockStyle.Fill
            tabPage.Select()
            Dim rcbx = New RichTextBox()
            rcbx.Parent = tabPage
            rcbx.Dock = DockStyle.Fill
            rcbx.Text = File.ReadAllText(filePath)
            tabPage.Controls.Add(rcbx)
            ColourRGBTextBox(rcbx)
        End If
    End Sub

    Private Sub CerrarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CerrarToolStripMenuItem.Click
        If TabControl1.TabPages.Count > 0 Then
            TabControl1.TabPages.Remove(TabControl1.SelectedTab)
        End If

    End Sub

    Private Sub ShellToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ShellToolStripMenuItem.Click
        MessageBox.Show("Version del Shell no disponible en la version actual.")
    End Sub

    Private Sub SalirToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles SalirToolStripMenuItem.Click
        Close()
    End Sub

    Private Sub ColourRGBTextBox(rcbx As RichTextBox)
        Dim regex As Regex = New Regex("\bFor\b|\bThen\b|\bIf\b|\bNext\b|\bElse\b|\bAs\b|\bWhere\b|\d|'(.*?)'", RegexOptions.IgnoreCase)
        For Each match As Match In regex.Matches(rcbx.Text)
            rcbx.Select(match.Index, match.Length)
            If IsNumeric(rcbx.SelectedText) Then
                rcbx.SelectionColor = Color.DarkCyan
            ElseIf rcbx.SelectedText.Contains("'") Then
                rcbx.SelectionColor = Color.Red
            Else
                rcbx.SelectionColor = Color.CornflowerBlue
            End If

        Next
        rcbx.Select(rcbx.TextLength, 0)
        rcbx.SelectionColor = Color.Black
    End Sub
    Private Sub GuardarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles GuardarToolStripMenuItem.Click
        Dim dialog As SaveFileDialog = New SaveFileDialog()
        Dim rtbx As RichTextBox = TabControl1.SelectedTab.Controls(0)
        If dialog.ShowDialog() = DialogResult.OK Then
            Dim filePath As String = dialog.FileName
            File.WriteAllLines(filePath, rtbx.Lines)
        End If
    End Sub

    Private Sub rcbx_TextChanged(sender As Object, e As EventArgs)
        Dim rcbx As RichTextBox = sender
        If rcbx.Equals(CType(TabControl1.SelectedTab.Controls(0), RichTextBox)) Then
            lblLines.Text = rcbx.Lines.Count()
        End If
        ColourRGBTextBox(rcbx)
    End Sub

    Private Sub CopiarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarToolStripMenuItem.Click
        Dim rcbx As RichTextBox = TabControl1.SelectedTab.Controls(0)
        rcbx.Copy()
    End Sub

    Private Sub CortarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CortarToolStripMenuItem.Click
        Dim rcbx As RichTextBox = TabControl1.SelectedTab.Controls(0)
        rcbx.Cut()
    End Sub

    Private Sub PegarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PegarToolStripMenuItem.Click
        Dim rcbx As RichTextBox = TabControl1.SelectedTab.Controls(0)
        rcbx.Paste()
    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AcercaDeToolStripMenuItem.Click

    End Sub

    Private Sub TabControl1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles TabControl1.SelectedIndexChanged
        lblLines.Text = CType(TabControl1.SelectedTab.Controls(0), RichTextBox).Lines.Count()
    End Sub
End Class
